<?php
/* $Id: styles.php,v 0.0.0.2 22/11/2004 18:02:07 mdb Exp $
 * $Author: mdb $
 *
 * InsanePHP Stylesheet Switcher Script
 * php released under PHP License - http://www.php.net/license/3_0.txt
 *
 * Copyright (c) 2004 Filippo Baruffaldi <mdb AT insaneminds DOT org> <http://www.insaneminds.org>
 * All associated graphics Copyright (c) 2004 Filippo Baruffaldi
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/

$expire = "2114398800";
$HTTP_REFERER = $_SERVER["HTTP_REFERER"];
$siteskin = $_COOKIE['siteskin'];

$v=0;
foreach($_GET as $key => $value) {                             //
        if ($v == 0) $fvar = $key;                             // Taking name of first element
        $v++;                                                  //
}

function changeskin($skin)
{
        global $HTTP_REFERER, $expire;
        setcookie ("siteskin");
        setcookie('siteskin', $skin, $expire);
        echo '<html>
        			<head>
        					<title>Changing Skin In Progress ...</title>
        					<meta http-equiv="Refresh" content="1;url='.$HTTP_REFERER.'">
        			</head>

        			<body>
        			</body>
        	</html>';
}

function getmicrotime()
{
         list($usec, $sec) = explode(" ",microtime());
         return ((float)$usec + (float)$sec);
}

function getclient_style($soft, $siteskin)
{
        global $newstyle, $jpaste, $fvar;

        $soft2 = explode(" ", $soft);  // Riconoscimento
        $s = explode("/", $soft2[9]);  // Software

        if ($siteskin == "white") {

                if ($_SERVER['SCRIPT_NAME'] == '/no-paste/index.php' && $fvar >> '0') {

                        if ( $s[0] == "MSIE" || $soft2[2] == "MSIE" ) {
                                $softw = "MSIE";
                                return "                \t\t<link rel='stylesheet' href='css/osxstyle-ie.css' type='text/css' />\n";

                        } elseif ( $s[0] == "Firefox" ) {
                                $softw = "Firefox";
                                return "                \t\t<link rel='stylesheet' href='css/osxstyle-ff.css' type='text/css' />\n";

                        } else {
                                $softw = "Compliant";
                                return "                \t\t<link rel='stylesheet' href='css/osxstyle.css' type='text/css' />\n";

                        }

                } else {

                        if ( $s[0] == "MSIE" || $soft2[2] == "MSIE" ) {
                                $softw = "MSIE";
                                return "                \t\t<link rel='stylesheet' href='http://www.insaneminds.org/snake/css/osxstyle-ie.css' type='text/css' />\n";

                        } elseif ( $s[0] == "Firefox" ) {
                                $softw = "Firefox";
                                return "                \t\t<link rel='stylesheet' href='http://www.insaneminds.org/snake/css/osxstyle-ff.css' type='text/css' />\n";

                        } else {
                                $softw = "Compliant";
                                return "                \t\t<link rel='stylesheet' href='http://www.insaneminds.org/snake/css/osxstyle.css' type='text/css' />\n";

                        }
                }

        } elseif ($siteskin == 'text') {
                return '';
        } else {

                if ($_SERVER['SCRIPT_NAME'] == '/no-paste/index.php' && $fvar >> '0') {

                        if ( $s[0] == "MSIE" || $soft2[2] == "MSIE" ) {
                                $softw = "MSIE";
                                return "                \t\t<link rel='stylesheet' href='css/style-ie.css' type='text/css' />\n";

                        } elseif ( $s[0] == "Firefox" ) {
                                $softw = "Firefox";
                                return "                \t\t<link rel='stylesheet' href='css/style-ff.css' type='text/css' />\n";

                        } else {
                                $softw = "Compliant";
                                return "                \t\t<link rel='stylesheet' href='css/style.css' type='text/css' />\n";

                        }

                } else {

                        if ( $s[0] == "MSIE" || $soft2[2] == "MSIE" ) {
                                $softw = "MSIE";
                                return "                \t\t<link rel='stylesheet' href='http://www.insaneminds.org/snake/css/style-ie.css' type='text/css' />\n";

                        } elseif ( $s[0] == "Firefox" ) {
                                $softw = "Firefox";
                                return "                \t\t<link rel='stylesheet' href='http://www.insaneminds.org/snake/css/style-ff.css' type='text/css' />\n";

                        } else {
                                $softw = "Compliant";
                                return "                \t\t<link rel='stylesheet' href='http://www.insaneminds.org/snake/css/style.css' type='text/css' />\n";

                        }
                }
        }
}
?>
