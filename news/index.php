<?php
/* $Id: index.php,v 0.0.1.2 22/11/2004 18:02:07 mdb Exp $
 * $Author: mdb $
 *
 * SnakeServices Site News-Reader Script
 * php released under PHP License - http://www.php.net/license/3_0.txt
 *
 * Copyright (c) 2004 Filippo Baruffaldi <mdb AT insaneminds DOT org> <http://www.insaneminds.org>
 * All associated graphics Copyright (c) 2004 Filippo Baruffaldi
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/

$HTTP_REFERER = $_SERVER["HTTP_REFERER"];                      // Taking URL for auto-redirecting
$REQUEST = $_SERVER['SCRIPT_FILENAME'];                        // Taking script filename for dont recycle header/footer

$v=0;
foreach($_GET as $key => $value) {                             //
 	$$key = $value;                                        // Put $_GET elements into variables
        if ($v == 0) $fvar = $key;                             // Taking name of first element
        $v++;                                                  //
}                                                              //


if ($REQUEST != '/var/www/insaneminds.biz/snake/index.php' && $REQUEST != '/var/www/insaneminds.biz/snake/admin/news.php') {
include '../mask.php';                                     //
$magicc = 1;                                                   // Includes
}      

  /* Function List */

function view_article($id)
{
        global $mhost, $muser, $mpass, $mdb, $tbnews, $HTTP_REFERER;

        $str = mysql_connect($mhost, $muser, $mpass);
        mysql_select_db($mdb);
        $rslt = select($tbnews, '`id`,`date`,`title`,`article`', "`id` = $id", $maxn, 'id');
        $line = mysql_fetch_array($rslt, MYSQL_ASSOC);

        if ($line['article'] != '') {
            echo '<p style="text-align: left; margin: 0px 30px;">';
            print $line['article'];
            echo '</p><br /><br /><a href="'.$HTTP_REFERER.'">return to previous page</a>';

        } else {
                echo ':: The article '.$id." doesn't exist ::";
                if (!empty($HTTP_REFERER)) echo '<meta http-equiv="Refresh" content="3;url='.$HTTP_REFERER.'">';
        }

}

function generate_news($maxn)
{
        global $mhost, $muser, $mpass, $mdb, $tbnews, $fvar, $magic, $REQUEST,$fvar;

	echo '                        
                                  <table id="newsi">';

		if ($fvar == "display") echo "<div class='tbox3'>";

        $str = mysql_connect($mhost, $muser, $mpass);
        mysql_select_db($mdb);
        $rslt = select($tbnews, '`id`,`date`,`title`,`article`,`foto`', $where, $maxn, '`id`');

        while ($linea = mysql_fetch_array($rslt, MYSQL_ASSOC)) {
             print "\t<tr>\n";
             if ($REQUEST == '/var/www/insaneminds.biz/admin/news.php') echo "<td><b>".$linea['id']."</b></td>";
             print "\t\t<td style='text-align: left;border-bottom: 1px solid #333'>".$linea['date']."</td><td style='border-bottom: 1px solid #333;'>".$linea['title'];
             if ($linea['article'] != '') {
                  print '';
                  print "</td>\n\t</tr>\n</table>\n<table id='newsi'>\t<tr>\n\t\t<td style='text-align: left;'>\n\t\t\t" . substr($linea['article'], 0, 150) . "&nbsp;<a href='http://www.insaneminds.org/snake/news/?".$linea['id']."'>read more...</a>\n\t\t</td>\n\t\t<td>\n\t\t\t<img src='http://www.insaneminds.org/snake/news/" . $linea['foto'] . "' alt=''>\t\t</td>\n\t</tr>\n</table>\n<br /><table id='newsi'>";
             } else {
               	  print "</td>\n\t</tr>\n";
             }
        }

        closedb($rslt, $str);

        echo '                                                            </table>
                                                                          ';
		if ($fvar == "display") { echo "</div>"; } else { if (!empty($maxn)) echo '<a href="http://www.insaneminds.org/snake/news/?display">all news</a>'; }
}

/* Switch operation to right function */

/* Switch operation to right function */

if (isset($display)) {
            if ( $magicc == 1 ) {
                      $site_page = "News :: Show all news";
                      head($site_page);
            }
            generate_news($maxn);

            if ( $magicc == 1 ) {
                      foot();
            }

} elseif ($fvar == "view") {
            $site_page = "News :: View article n� $view";
            head($site_page);
            view_article($view);
            foot();

} elseif (!isset($fvar)) {
            if ( $magicc == 1 ) {
                      $site_page = "News";
                      head($site_page);
            }

            generate_news("10");

            if ( $magicc == 1 ) {
                      foot();
            }

} else {
            $site_page = "News :: View article n� $fvar";
            head($site_page);
            view_article($fvar);
            foot();

}