<?php
/* $Id: index.php,v 0.0.0.1 13/12/2004 01:54:07 mdb Exp $
 * $Author: mdb $
 *
 * GeekServices.Net E-Commerce Site Main Mask Functions
 * php released under PHP License - http://www.php.net/license/3_0.txt
 *
 * Copyright (c) 2004 Filippo Baruffaldi <mdb AT insaneminds DOT org> <http://www.insaneminds.org>
 * All associated graphics Copyright (c) 2004 Filippo Baruffaldi
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/

include 'includes.php';

function head($page) {

         global $soft, $siteskin, $site_name;

         print "                <?xml version='1.0' encoding='iso-8859-1' ?>\n
                <!DOCTYPE html\n
                \tPUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'\n
                \t'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>\n
                \n
                <html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>\n
                \t<head>\n
                \t\t<title>" . $site_name . "</title>\n
                \t\t<meta name='metadata'   content='ownsoft' />\n
                \t\t<meta name='subject'    content='Internet Services Provider' />\n
                \t\t<meta name='title'      content='SnakeServices: the way to dreaming' />\n
                \t\t<meta name='author'     content='Filippo Baruffaldi' />\n
                \t\t<meta name='publisher'  content='SnakeServices.Net' />\n
                \t\t<meta name='date'       content='15Nov2004' />\n
                \t\t<meta name='form'       content='xhtml' />\n
                \t\t<meta http-equiv='Keywords' content='snakeservices,hosting,web hosting,shell hosting,vpn,internet services' />\n
                \t\t<meta http-equiv='Description' content='Internet Services Provider' />\n
                \t\t<meta http-equiv='Content-Type' content='text/xhtml; charset=iso-8859-1' />\n
                \t\t<link rev='made' href='mailto:mdb@insaneminds.org' />\n";
                
                print getclient_style($soft, $siteskin);
                
         print "\n                \t</head>\n
                \t<body onload=".'"'."window.defaultStatus='SnakeServices.net :: Internet Services Provider';".'"'.">\n
                \n
                \t\t<!--\n
                \t\tSite Main Layer\n
                \t\t-->\n
                \n
                \t\t\t<div class='container'>\n
                \n
                \t\t<!--\n
                \t\tHeader Informations\n
                \t\t-->\n
                \n
                \t\t\t\t<div class='header'>\n
                \t\t\t\t</div>\n
                \t\t<!--\n
                \t\tSubHeader Informations\n
                \t\t-->\n
                \n
                \t\t\t\t<div class='location'>\n
                \t\t\t\t\t".'" blah blah blah blah blah blah blah blah blah "'."<br />\n
                \t\t\t\t\t( blah blah blah blah blah blah blah blah blah blah blah blah blah blah, blah )<br />\n
                \t\t\t\t</div>\n
                \n
                \t\t<!--\n
                \t\tVertical Left SideBar Informations\n
                \t\t-->\n
                \n
                \t\t\t\t<div class='navigation'>\n
                \n
                \t\t<!--\n
                \t\tSideBar Window Informations\n
                \t\t-->\n
                \n
                \t\t\t\t\t<div class='minicontainer'>\n
                \n
                \t\t\t\t\t\t<div class='bar'>\n
                \n
                \t\t\t\t\t\t\t<div class='menutitle'>\n
                \t\t\t\t\t\t\t\t<h1>Snake Services</h1>\n
                \t\t\t\t\t\t\t</div>\n
                \n
                \t\t\t\t\t\t</div>\n
                \n
                \t\t\t\t\t\t<div class='bar'>\n
                \n
                \t\t\t\t\t\t\t<div class='menucontent'>\n
                \t\t\t\t\t\t\t\t<table>\n
                \n
                \t\t\t\t\t\t\t\t\t<tr>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<img src='http://www.insaneminds.org/snake/imgs/menu.general.news.gif' alt='News' />\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<a href='http://www.insaneminds.org/snake/news'>news</a>\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t</tr>\n
                \n
                \t\t\t\t\t\t\t\t\t<tr>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<img src='http://www.insaneminds.org/snake/imgs/menu.general.contacts.gif' alt='Contacts' />\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<a href='http://www.insaneminds.org/snake/?contacts'>contacts</a>\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t</tr>\n
                \n
                \t\t\t\t\t\t\t\t\t<tr>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<img src='http://www.insaneminds.org/snake/imgs/menu.hosting.shell.gif' alt='Shell Hosting' />\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<a href='http://www.insaneminds.org/snake/?underconstruction'>shell hosting</a>
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t</tr>\n
                \n
                \t\t\t\t\t\t\t\t\t<tr>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<img src='http://www.insaneminds.org/snake/imgs/menu.hosting.web.gif' alt='Web Hosting' />\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<a href='http://www.insaneminds.org/snake/?underconstruction'>web hosting</a>
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t</tr>\n
                \n
                \t\t\t\t\t\t\t\t\t<tr>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<img src='http://www.insaneminds.org/snake/imgs/menu.hosting.domain.gif' alt='Domain Hosting' />\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<a href='http://www.insaneminds.org/snake/?underconstruction'>domain hosting</a>
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t</tr>\n
                \n
                \t\t\t\t\t\t\t\t\t<tr>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<img src='http://www.insaneminds.org/snake/imgs/menu.hosting.email.gif' alt='Email Hosting' />\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<a href='http://www.insaneminds.org/snake/?underconstruction'>email hosting</a>\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t</tr>\n
                \n
                \t\t\t\t\t\t\t\t\t<tr>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<img src='http://www.insaneminds.org/snake/imgs/menu.hosting.vpn.gif' alt='Vpn Hosting' />\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<a href='http://www.insaneminds.org/snake/?underconstruction'>vpn hosting</a>\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t</tr>\n
                \n
                \t\t\t\t\t\t\t\t\t<tr>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<img src='http://www.insaneminds.org/snake/imgs/menu.hosting.gameserver.gif' alt='Game Server Shell Hosting' />\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<a href='http://www.insaneminds.org/snake/?underconstruction'>gameserver</a>\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t</tr>\n
                \n
                \t\t\t\t\t\t\t\t\t<tr>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<img src='http://www.insaneminds.org/snake/imgs/menu.hosting.dedicated.gif' alt='Dedicated Server' />\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<a href='http://www.insaneminds.org/snake/?underconstruction'>dedicated</a>\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t</tr>\n
                \n
                \t\t\t\t\t\t\t\t\t<tr>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<img src='http://www.insaneminds.org/snake/imgs/menu.services.vhosts.gif' alt='Available Vhosts' />\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<a href='http://www.insaneminds.org/snake/?underconstruction'>vhosts</a>\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t</tr>\n
                \n
                \t\t\t\t\t\t\t\t\t<tr>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<img src='http://www.insaneminds.org/snake/imgs/menu.services.statistics.gif' alt='Servers Statistics' />\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<a href='http://www.insaneminds.org/snake/?underconstruction'>statistics</a>\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t</tr>\n
                \n
                \t\t\t\t\t\t\t\t</table>\n
                \t\t\t\t\t\t\t</div>\n
                \n
                \t\t\t\t\t\t</div>\n
                \n
                \t\t\t\t\t</div>\n
                \n
                \t\t\t\t\t<div class='minicontainer'>\n
                \n
                \t\t\t\t\t\t<div class='bar'>\n
                \n
                \t\t\t\t\t\t\t<div class='menutitle'>\n
                \t\t\t\t\t\t\t\t<h1>Links</h1>\n
                \t\t\t\t\t\t\t</div>\n
                \n
                \t\t\t\t\t\t</div>\n
                \n
                \t\t\t\t\t\t<div class='bar'>\n
                \n
                \t\t\t\t\t\t\t<div class='menucontent'>\n
                \t\t\t\t\t\t\t\t<table>\n
                \n
                \t\t\t\t\t\t\t\t\t<tr>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<img src='http://www.insaneminds.org/snake/imgs/insane.gif' alt='InsaneMinds Community' />\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<a href='http://www.insaneminds.org'>insaneminds</a>\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t</tr>\n
                \n
                \t\t\t\t\t\t\t\t\t<tr>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<img src='http://www.insaneminds.org/snake/imgs/necta.gif' alt='Nectarine`s Email' />\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t\t<td>\n
                \t\t\t\t\t\t\t\t\t\t\t<a href='http://www.nectarine.it'>nectarine</a>\n
                \t\t\t\t\t\t\t\t\t\t</td>\n
                \t\t\t\t\t\t\t\t\t</tr>\n
                \n
                \t\t\t\t\t\t\t\t</table>\n
                \t\t\t\t\t\t\t</div>\n
                \n
                \t\t\t\t\t\t</div>\n
                \n
                \t\t\t\t\t</div>\n
                \n
                \t\t\t\t\t<div class='minicontainer'>\n
                \n
                \t\t\t\t\t\t<div class='bar'>\n
                \n
                \t\t\t\t\t\t\t<div class='menutitle'>\n
                \t\t\t\t\t\t\t\t<h1>Banners</h1>\n
                \t\t\t\t\t\t\t</div>\n
                \n
                \t\t\t\t\t\t</div>\n
                \n
                \t\t\t\t\t\t<div class='bar'>\n
                \n
                \t\t\t\t\t\t\t<div class='menucontent'>\n
                \t\t\t\t\t\t\t\t<a href='http://validator.w3.org/check?uri=referer'><img src='http://www.insaneminds.org/snake/imgs/menu.spot.xhtml.png' alt='W3C XHTML Validation!' /></a><br />\n
                \t\t\t\t\t\t\t\t<a href='http://jigsaw.w3.org/css-validator/validator?uri=http://www.insaneminds.org/snake/'><img src='http://www.insaneminds.org/snake/imgs/menu.spot.css.png' alt='W3C CSS Validation!' /></a><br />\n
                \t\t\t\t\t\t\t\t<a href='http://www.snakeservices.net/?rss'><img src='http://www.insaneminds.org/snake/imgs/menu.spot.rss.png' alt='RSS Version!' /></a><br />\n
                \t\t\t\t\t\t\t\t<a href='http://www.NoSoftwarePatents.com/'><img src='http://www.insaneminds.org/snake/imgs/menu.spot.nopat.gif' alt='No Software Patents!' /></a>\n
                \t\t\t\t\t\t\t</div>\n
                \n
                \t\t\t\t\t\t</div>\n
                \n
                \t\t\t\t\t</div>\n
                \n
                \t\t\t\t</div>\n
                \n
                \t\t<!--\n
                \t\tContent Central Box Informations\n
                \t\t-->\n
                \n
                \t\t\t\t<div class='content'>\n
                \n
                \t\t<!--\n
                \t\tContent Window Informations\n
                \t\t-->\n
                \n
                \t\t\t\t\t<div class='ccontainer'>\n
                \n
                \t\t\t\t\t\t<div class='foo'>\n
                \n
                \t\t\t\t\t\t\t<div class='ctitle'>\n
                \t\t\t\t\t\t\t\t<h1>:: " . $page . " ::</h1>\n
                \t\t\t\t\t\t\t</div>\n
                \n
                \t\t\t\t\t\t</div>\n
                \n
                \t\t\t\t\t\t<div class='foo'>\n
                \n
                \t\t\t\t\t\t\t<div class='ccontent'>\n";
}


function foot() 
{

         print "                \t\t\t\t\t\t\t</div>\n
                \n
                \t\t\t\t\t\t</div>\n
                \n
                \t\t\t\t\t</div>\n
                \n
                \t\t\t\t</div>\n
                \n
                \t\t<!--\n
                \t\tFooter Informations\n
                \t\t-->\n
                \n
                \t\t\t\t<div class='footer'>\n
                \t\t\t\t\t<p>Copyright &copy; 2004-2005 Snake Services Inc, All rights reserved.</p>\n
                \t\t\t\t\t<a href='http://www.snakeservices.net/'>SnakeServices.net Home</a> ::\n
                \t\t\t\t\t<a href='http://www.snakeservices.net/?sitemap'>Site Map</a> ::\n
                \t\t\t\t\t<a href='http://www.snakeservices.net/?faq'>Faq</a> ::\n
                \t\t\t\t\t<a href='http://www.snakeservices.net/?contacts'>Contact Us</a> ::\n
                \t\t\t\t\t<a href='http://www.snakeservices.net/?privacy'>Privacy Policy</a> ::\n
                \t\t\t\t\t<a href='http://www.snakeservices.net/?tos'>Terms &amp; Conditions</a>\n
                \t\t\t\t</div>\n
                \n
                \t\t\t</div>\n
                \n
                \t</body>\n
                \n
                </html>\n";
}


function contacts()
{
         print "                \t\t\t\t\t\t\t\t<div class='lists'>\n
                \t\t\t\t\t\t\t\t\t<dl>\n
                \t\t\t\t\t\t\t\t\t\t<dt>Please feel free to contact any of our departments, soon as possible i will code a PHP module to optimize the support system. Now you can only send us an e-mail and we will be happy to answer your question as quickly as possible.</dt>\n
                \t\t\t\t\t\t\t\t\t\t<dd>Info: info[at]snakeservices.net</dd>\n
                \t\t\t\t\t\t\t\t\t\t<dd>Sales: sales[at]snakeservices.net</dd>\n
                \t\t\t\t\t\t\t\t\t\t<dd>Billing: billing[at]snakeservices.net</dd>\n
                \t\t\t\t\t\t\t\t\t\t<dd>Support: support[at]snakeservices.net</dd>\n
                \t\t\t\t\t\t\t\t\t\t<dd>Network Administrator: admin[at]snakeservices.net</dd>\n
                \t\t\t\t\t\t\t\t\t\t<dd>Abuse: abuse[at]snakeservices.net</dd>\n
                \t\t\t\t\t\t\t\t\t\t<dt>You can also reach us in real-time with available staff members on IRC (Internet Relay Chat) trought <a style='font: 13px' href='http://www.freenode.net'>FreeNode Network</a> at this addresses:</dt>\n
                \t\t\t\t\t\t\t\t\t\t<dd>Server IPv4: irc.freenode.net</dd>\n
                \t\t\t\t\t\t\t\t\t\t<dd>Server IPv6: irc6.freenode.net</dd>\n
                \t\t\t\t\t\t\t\t\t\t<dd>Channel: #snakeservices</dd>\n
                \t\t\t\t\t\t\t\t\t\t<dt>If you haven`t a client IRC you can use <a style='font: 13px' href='http://www.nectarine.it/index.php?gadget=blog&amp;action=single_view&amp;id=6'>Nectarine`s WebChat</a>.</dt>\n
                \t\t\t\t\t\t\t\t\t\t<dd>URL: www.nectarine.it</dd>\n
                \t\t\t\t\t\t\t\t\t</dl>\n
                \t\t\t\t\t\t\t\t</div>\n";
}

function underconst()
{
         print "                \t\t\t\t\t\t\t\t:: The section is under construction ::\n";
}

function underupdate()
{
         print "                \t\t\t\t\t\t\t\t:: The section is under updating ::\n";
}

function permforb()
{
         print "                \t\t\t\t\t\t\t\t:: Permission FORBIDDEN ::\n";
}
?>