<?php
/* $Id: defines.php,v 0.0.0.3 22/11/2004 18:02:07 mdb Exp $
 * $Author: mdb $
 *
 * InsanePHP Global Variables Settings
 * php released under PHP License - http://www.php.net/license/3_0.txt
 *
 * Copyright (c) 2004 Filippo Baruffaldi <mdb AT insaneminds DOT org> <http://www.insaneminds.org>
 * All associated graphics Copyright (c) 2004 Filippo Baruffaldi
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/

$site_name = "SnakeServices.net * Internet Services Provider :: ";
$site_struct = 'original';

/*
 * Do NOT edit below
*/

$unixdate = '';
$date = date("d/m/y");
$time = explode(':', gmstrftime("%T", time()));
$time = $time[0].':'.$time[1];
$ip = $_SERVER["REMOTE_ADDR"];
$soft = $_SERVER["HTTP_USER_AGENT"];
$HTTP_REFERER = $_SERVER["HTTP_REFERER"];
$REQUEST_URI = $_SERVER["REQUEST_URI"];
