<?php
/* $Id: index.php,v 0.0.0.1 24/11/2004 01:54:07 mdb Exp $
 * $Author: mdb $
 *
 * GeekServices.Net E-Commerce Site Main File
 * php released under PHP License - http://www.php.net/license/3_0.txt
 *
 * Copyright (c) 2004 Filippo Baruffaldi <mdb AT insaneminds DOT org> <http://www.insaneminds.org>
 * All associated graphics Copyright (c) 2004 Filippo Baruffaldi
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/
// HTTP/1.1
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

// HTTP/1.0
header("Pragma: no-cache");

// Includes
include 'mask.php';

$v=0;
foreach($_GET as $key => $value) {                             //
 	$$key = $value;                                            // Put $_GET elements into variables
        if ($v == 0) $fvar = $key;                             // Taking name of first element
        $v++;                                                  //
}                                                              //

$time_start = getmicrotime();                                  // Timing rendering page

// Switch section script
switch($fvar) {
		default:
              $site_page = "News";
              head($site_page);
		      require 'news/index.php';
		      foot();
		      break;

     	case "home":
          $site_page = "News";
		  head($site_page);
		  require 'news/index.php';
		  foot();
		  break;

     	case "contacts":
                  unset($site_page);
                  $site_page = "Contact Us!";
		  $softw = head($site_page);
		  contacts();
		  foot();
		  break;
		  
     	case "underconstruction":
          $site_page = "Section Under Construction";
		  head($site_page);
		  underconst();
		  foot();
		  break;

     	case "underupdating":
          $site_page = "Section Under Updating";
		  head($site_page);
		  underupdate();
		  foot();
		  break;

     	case "forb":
          $site_page = "Forbidden !";
		  head($site_page);
		  permforb();
		  foot();
		  break;

     	case "tos";
          $site_page = "Terms &amp; Conditions of services";
		  head($site_page);
          snaketos();
		  foot();
		  break;

     	case "privacy";
          $site_page = "Privacy Policy";
		  head($site_page);
          snakeprivacy();
		  foot();
		  break;

     	case "trademark";
          $site_page = "TradeMark Informations";
		  head($site_page);
          snaketrademark();
		  foot();
		  break;

     	case "faq";
          $site_page = "Frequently Asked Questions";
		  head($site_page);
          snakefaq();
		  foot();
		  break;

     	case "vhosts";
          $site_page = "Available Vhosts";
		  head($site_page);
          pythonvhosts();
		  foot();
		  break;
		  
     	case "stats";
          $site_page = "Servers Information";
		  head($site_page);
          pythonstats();
		  foot();
		  break;
}
?>