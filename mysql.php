<?php
/* $Id: mysql.php,v 0.0.0.7 22/11/2004 18:02:07 mdb Exp $
 * $Author: mdb $
 *
 * InsanePHP MySQL Mini-FrameWork
 * php released under PHP License - http://www.php.net/license/3_0.txt
 *
 * Descrizione
 * Descrizione
 *
 * Copyright (c) 2004 Filippo Baruffaldi <mdb AT insaneminds DOT org> <http://www.insaneminds.org>
 * All associated graphics Copyright (c) 2004 Filippo Baruffaldi
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/
/*
 * Funzione Count rows/fields
 * Funzione Search
 * Password in mysql.conf md5 e decrypt on connect?
*/


function opendb($db)
{
        global $mhost, $muser, $mpass;
        $str = mysql_connect($mhost, $muser, $mpass)
           or die("Sorry! Can't connect on database");
        mysql_select_db($db);
        return $str;
}

function closedb($result, $str)
{
       if ($result != "") mysql_free_result($result);
       if (false !== (mysql_select_db('News', $str))) mysql_close($str);
}

function select($table, $col, $where, $limit, $order)
{
       $table = explode('?', $table);
       if ($table[1] == "DESC") {
               if (!empty($limit)) {
                     $query = "SELECT $col FROM $table[0] ORDER BY $order DESC LIMIT $limit";
                     if (!empty($where)) $query = "SELECT $col FROM $table[0] WHERE $where ORDER BY $order DESC LIMIT $limit";
               } else {
                     $query = "SELECT $col FROM $table[0] ORDER BY $order DESC";
                     if (!empty($where)) $query = "SELECT $col FROM $table[0] WHERE $where ORDER BY $order DESC";
               }
		} elseif ($table[1] == "debug") {
        	   print $query;
        } else {
               if (!empty($limit)) {
                     $query = "SELECT $col FROM $table[0] ORDER BY $order ASC LIMIT $limit";
                     if (!empty($where)) $query = "SELECT $col FROM $table[0] WHERE $where ORDER BY $order ASC LIMIT $limit";
               } else {
                     $query = "SELECT $col FROM $table[0] ORDER BY $order ASC";
                     if (!empty($where)) $query = "SELECT $col FROM $table[0] WHERE $where ORDER BY $order ASC";
               }
        }
        
       $result = mysql_query($query)
           or die('Sorry! Can`t work that query[select] on database');
       return $result;
}

function insertrow($table, $col, $values)
{
       $table = explode('?', $table);
       $query = "INSERT INTO $table[0] ($col) VALUES ($values)";
       if ($table[1] == 'debug') print $query;
       $result = mysql_query($query)
           or die('Sorry! Can`t work that query[insert] on database');
       return $result;
}

function deleterow($table, $where)
{
       $table = explode('?', $table);
       $query = "DELETE FROM $table[0] WHERE $where";
       if ($table[1] == 'debug') print $query;
       $result = mysql_query($query)
           or die('Sorry! Can`t work that query[delete] on database');
       return $result;
}

function printresult($result) {
   while ($linea = mysql_fetch_array($result, MYSQL_ASSOC)) {
       foreach ($linea as $valore_colonna) {
           print "$valore_colonna<br />\n";
       }
   }
}

function printresulttb($result) {
   while ($linea = mysql_fetch_array($result, MYSQL_ASSOC)) {
       print "\t<tr>\n";
       foreach ($linea as $valore_colonna) {
           print "\t\t<td>$valore_colonna</td>\n";
       }
       print "\t</tr>\n";
   }
}

function modifyrow($table, $col, $value, $where)
{
       $table = explode('?', $table);
       $query = "UPDATE $table[0] SET $col = ".'"'.$value.'"'." WHERE $where";
       if ($table[1] == 'debug') print $query;
       $result = mysql_query($query)
           or die('Sorry! Can`t work that query[update/set] on database');
       return $result;
}

function insane_sql_query($query)
{
       $query = explode('?', $query);
       if ($query[1] == 'debug') print $query;
       $result = mysql_query($query)
           or die('Sorry! Can`t work that query[select] on database');
       return $result;
}
?>
