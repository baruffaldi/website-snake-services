<?php
/* $Id: legal.php,v 0.0.0.1 16/12/2004 01:54:07 mdb Exp $
 * $Author: mdb $
 *
 * GeekServices.Net E-Commerce Site Legal Info Functions
 * php released under PHP License - http://www.php.net/license/3_0.txt
 *
 * Copyright (c) 2004 Filippo Baruffaldi <mdb AT insaneminds DOT org> <http://www.insaneminds.org>
 * All associated graphics Copyright (c) 2004 Filippo Baruffaldi
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/

function snaketos()
{

         print "                \t\t\t\t\t\t\t\t<h1> Terms and conditions to use our services </h1>\n
                \t\t\t\t\t\t\t\t\t<div class='lists'>\n
                \t\t\t\t\t\t\t\t\t\t<dl>\n
                \t\t\t\t\t\t\t\t\t\t\t<dt>1. Fees. Subscriber/Organization agrees to pay all the fees as set forth by agreement with Snake Services Inc. for standard or modified Internet services.</dt>\n
                \t\t\t\t\t\t\t\t\t\t\t<dt>2. Payment/Non-payment</dt>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* All accounts must be paid in full prior to activation or re-activation.</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* Accounts are billed monthly, quarterly, annually, depending on your plan.</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* Monthly Accounts will be billed in advance for services starting on that day and billed monthly from the start date of service.</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* Quarterly Accounts will be billed 3 months in advance.</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* Bi-Annual Accounts will be billed 6 months in advance.</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* Annual Accounts will be billed 10 months in advance.</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* All Payments are made by: PayPal, Credit Card, BancoPosta(Italian payments) account transfer or Western Union. Please make money orders and certified checks payable to: Snake Services Inc.</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* All Accounts will be continuously billed and maintained until the subscriber chooses to terminate said account.</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* Payment Due on the Invoice Date</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* Failure to pay a bill within 7 days of the billing date, or issuing bad credit card, checks or money orders, will result in a freeze on the delinquent account until the payment clears.</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* Frozen accounts will still be active and able to receive e-mail or other files, but will be inaccessible to the account holder for the duration of the suspension.</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* Any account which remains frozen for more than one week will be terminated.</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* Payment not received for back fees will be deleted and turned over to a collection agency after a period of 60 days. No refunds after 48h.</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* Subscriber agrees to pay the cost of collection of any unpaid charges due under the terms of Agreement, including reasonable attorney's fees and expenses.</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* Support is not available for accounts with open balances.</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dt>3. All account holders with Snake Services Inc. must be at least 18 years or older. Accounts may be formed in the name of a younger subscriber with the expressed signed, written consent of a parent or guardian, that parent or guardian is then solely responsible for the terms of the subscriber contract in full.</dt>\n
                \t\t\t\t\t\t\t\t\t\t\t<dt>4. Only the Subscriber/Organization signing this Agreement is allowed access to an account established for that subscriber/organization; service will be disconnected to accounts used by persons other than subscriber/organization. Subscriber/Organization agrees to access the Internet system for lawful purposes only and to comply with rules and regulations established by other networks via the system supplied by Snake Services Inc.</dt>\n
                \t\t\t\t\t\t\t\t\t\t\t<dt>5. Transmission of material in violation of any state or federal regulation is prohibited, including copyrighted material, material legally determined to be threatening or obscene, or material protected by trade secret (including Warez and/or pirated software). Eternal Networks Inc. reserves the right to withhold and/or remove World Wide Web pages or any and all other materials on our systems sponsored by the Subscriber/Organization if they contain any of the aforementioned items.</dt>\n
                \t\t\t\t\t\t\t\t\t\t\t<dt>6. Usage by Subscriber/Organization which could possibly result in damage to the hardware, software, or security of Snake Services Inc. and its network, subscribers or the Internet system shall result in immediate cancellation of Agreement without refund. Expressly prohibited activities include, but are not limited to: </dt>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* Downloading software on the Internet system via services supplied by Snake Services Inc. which interferes with operation of the Internet system and or Snake Services Inc. network system.</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* Manipulation of Internet system which allows operation of programs accessed via the Internet while not actively online.</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* The use of Snake Services Inc. servers, its client's servers, or any other network devices and/or network servers associated with Snake Services Inc. to be used to relay mail, or used in any illegal manner whatsoever which includes such items as SPAM, UCE (Unsolicited Commercial Email.)</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dt>7. Snake Services Inc. reserves the right to modify the terms and conditions contained in Agreement upon 30 days notice to Subscriber /Organization, transmitted via e-mail.</dt>\n
                \t\t\t\t\t\t\t\t\t\t\t<dt>8. Snake Services Inc. reserves the right to cancel Agreement at anytime without penalty.</dt>\n
                \t\t\t\t\t\t\t\t\t\t\t<dt>9. Snake Services Inc. makes no warranties of any kind, either expressed or implied, for the services it provides. Snake Services Inc. specifically denies any responsibility whatsoever for the accuracy or quality of information obtained through its services, and use of information is at risk of Subscriber/Organization. Subscriber/Organization agrees to indemnify and hold Snake Services Inc. not responsible for any use by Subscriber/Organization and/or its employees which constitutes an illegal activity. Snake Services Inc. is further not responsible for the contents of its users homepages and or illegal, obscene or harmful material. Under no circumstances is Snake Services Inc. responsible for the status, altered or current, of any data stored or transmitted in any of its facilities. Subscriber/Organization agrees to indemnify and hold not responsible Snake Services Inc. from any claims resulting from use of service which causes damage or loss to the Subscriber/Organization, or any other party, including, but not limited to loss of data resulting from delays, nondelivery of information, or service interruptions. In addition, the subscriber understands that the use of Snake Services Inc. facilities includes the possible risk of a damaging event such as weather, electrical surge, theft, line failure, viruses that you get, or other acts of God that could affect data or access availability. Snake Services Inc. is not liable for any losses caused by such events or occurrences.</dt>\n
                \t\t\t\t\t\t\t\t\t\t\t<dt>10. Snake Services Inc. shall issue a password to Subscriber/Organization to preserve confidentiality. Subscriber/Organization is responsible for protection of the password and agrees to indemnify and hold Snake Services Inc. not responsible for any damages resulting from an account password compromised by a Subscriber/Organization. Snake Services Inc. agrees not to cause a breach of the confidentiality of information passing through its services, except by court order, but reserves the right to view confidential information only when necessary to troubleshoot and correct any network/hardware problems associated with the Subscriber/Organization's device/s.</dt>\n
                \t\t\t\t\t\t\t\t\t\t\t<dt>11. Acceptable use for shell services are strongly enforced:</dt>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* IRCd (IRC server) accounts maximum users limit needs to be correctly set up in the IRCd configuration file. We reserve the right to go through any configuration file on our servers. In the case that the IRC server is a hub, the clients on the leaf servers counts as users.</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dd>* Bouncers programs are limited to 1 user / 1 IRC server connection. If abuse is detected, the account will be suspended without notice.</dd>\n
                \t\t\t\t\t\t\t\t\t\t\t<dt>12. This Agreement is governed under the laws of the State of Italy. If any provision of Agreement is held to be invalid, illegal, void, or for any other reason unenforceable, that provision shall be severed without affecting the remaining provisions, and the remaining provisions shall remain in full force and effect. This Agreement supersedes any and all previous representations, understandings or agreements.</dt>\n
                \t\t\t\t\t\t\t\t\t\t</dl>\n
                \t\t\t\t\t\t\t\t\t</div>\n";
}

function snakeprivacy()
{
         print "                \t\t\t\t\t\t\t\t<h1> Privacy Policy </h1>\n
         	    \t\t\t\t\t\t\t\t\tSnake Services Inc. values your privacy.<br />\n
         	    \t\t\t\t\t\t\t\t\tInformation you provide at any time to the company will never be sold, exchanged or released to any third party entity, unless required by court order, subpoena, or other legal force.\n
		 	    \t\t\t\t\t\t\t\t\t<div class='lists'>\n
                \t\t\t\t\t\t\t\t\t\t<dl>\n
                \t\t\t\t\t\t\t\t\t\t\t<dt>The Collection and Use of Information</dt>\n
				\t\t\t\t\t\t\t\t\t\t\t<dd>In order to protect your information, we maintain and enforce strict security procedures.</dd>\n
				\t\t\t\t\t\t\t\t\t\t\t<dd>All sensitive transactions use Secure Socket Layer (SSL) encrypted network transmissions.</dd>\n
				\t\t\t\t\t\t\t\t\t\t\t<dd>We collect personal information about you (Name, Address, etc...) from our online forms or telephone conversations.</dd>\n
				\t\t\t\t\t\t\t\t\t\t\t<dd>Any personal information we gather is needed to conduct our business relationship and for verification purposes.</dd>\n
				\t\t\t\t\t\t\t\t\t\t\t<dd>We use state-of-the-art technology to keep your personal information including your billing and account information-as secure as possible. Credit Card information is not stored online.</dd>\n
				\t\t\t\t\t\t\t\t\t\t\t<dd>No account information will be released to any party unless the proper security checks have been performed.</dd>\n
				\t\t\t\t\t\t\t\t\t\t\t<dd>Company will only disclose information if the member has consented to the disclosure.</dd>\n
				\t\t\t\t\t\t\t\t\t\t\t<dd>Company will not disclose customer information or activities to another site or entity unless required by court order, subpoena, or other legal force.</dd>\n
				\t\t\t\t\t\t\t\t\t\t\t<dt>Using our website</dt>
				\t\t\t\t\t\t\t\t\t\t\t<dd>All of our cookies and sessions are temporary, which means that they are active only as long as the member`s browser is running. Cookies and sessions expire when the session has ended.</dd>\n
				\t\t\t\t\t\t\t\t\t\t\t<dd>Company gathers non-personal information from your browser when using our website. This type of information is used to generate statistics, keep track of your visit and internal purposes</dd>\n
				\t\t\t\t\t\t\t\t\t\t\t<dt>Communications</dt>
				\t\t\t\t\t\t\t\t\t\t\t<dd>When submitting comments or reviews via online forms or email you agree to allow Snake Services Inc. to use quotes as we see fit.</dd>\n
				\t\t\t\t\t\t\t\t\t\t\t<dd>Any complaints submitted to our abuse department will remain anonymous. At no time will your contact information be disclosed to the offending parties.</dd>\n
				\t\t\t\t\t\t\t\t\t\t\t<dd>Information transmitted over the Internet and/or other computer networks is generally not considered to be secure. While Provider respects the privacy of our customers, the customer understands that there is no guaranteed privacy on the Internet. The provider cannot be liable or for any viewing or interception of the customer`s e-mail, downloads, news, etc., by other customers or individuals at the providers network or on any other network.</dd>\n
				\t\t\t\t\t\t\t\t\t\t</dl>
				\t\t\t\t\t\t\t\t\t</div>
				\t\t\t\t\t\t\t\t\tSnake Services Inc.<br />\n
				\t\t\t\t\t\t\t\t\tVia A. Liri 31<br />\n
				\t\t\t\t\t\t\t\t\tGenova, GE 16145<br />\n
				\t\t\t\t\t\t\t\t\tItaly\n
				";
}

function snakefaq()
{
         print "                \t\t\t\t\t\t\t\t\n
		 	                    \t\t\t\t\t\t\t\t\n";
}

?>